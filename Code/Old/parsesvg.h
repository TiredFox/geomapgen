#ifndef PARSESVG_H
#define PARSESVG_H

#include <string>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <sstream>

/**
 * @brief This class will take a svg and split its path components into a json format.
 * This is purely a tool for this project and is not a key factor of this project
 */
class parsesvg{
public:
  void parseToJSON(std::string _fileName); 
private:
  std::string items[3];
};

#endif //PARSESVG_H
