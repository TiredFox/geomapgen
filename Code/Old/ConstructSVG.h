#ifndef CONSTRUCTSVG_H
#define CONSTRUCTSVG_H
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

/**
 * @brief Holds a id,class,and d for a path element of an svg
 * in the form of strings
 */
struct County{
public:
  std::string id;
  std::string _class;
  std::string d;
};

/**
* @brief Will hold all of the information for running and generating one map
*/
struct Actions{
public:
  std::string label;
  std::string t1;
  std::string t2;
  std::string t3;
  std::string t4;
  std::string dataFile;
};

/**
 * @brief This class is used to construct a svg from
 * a JSON and data file. This svg is a geomap of wisconsin
 */
class ConstructSVG{
public:
bool pullCounties(std::string); /*! Will pull the counties from a given JSON file */
bool readData(ConstructSVG&, std::string,int); /*! Will read in data from file and translate/calculate it */
bool applyData(ConstructSVG&,std::string,int); /*! Will apply the calculated data to the counties */
bool genSVG(ConstructSVG&,std::string, int); /*! Will generate a SVG based on the manipulated counties. Made to do multiple maps*/
bool convertSVGtoPNG(); /*! Will take the svg and convert to png? Don't know how to do this */
void parseToken(std::string&); /*! Helper function that will split a token out of the string="value" */
void insertAction(Actions _action) {actions.push_back(_action);} /*! adds a action to the actions vector */
int getNumMaps(){return actions.size();} /*! returns the total number of maps that will be generatred */
void printCounties(){
  for(int i=0; i < counties.size();++i)
  {
    std::cout << counties[i].id << " " << counties[i]._class << std::endl;
  }
}
std::string getDataFile(int index){return actions[index].dataFile;} /*! Return the data file name from actions vector*/
private:
  std::vector<County> counties;
  std::vector<Actions> actions;
  int MAX = 0;
  int r1=0,r2=0,r3=0;
};

#endif //CONSTRUCTSVG_H
