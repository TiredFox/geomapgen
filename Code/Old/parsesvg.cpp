#include "parsesvg.h"

const std::string WHITESPACE = " \n\r\t\f\v";
 
std::string ltrim(const std::string &s)
{
    size_t start = s.find_first_not_of(WHITESPACE);
    return (start == std::string::npos) ? "" : s.substr(start);
}
 
std::string rtrim(const std::string &s)
{
    size_t end = s.find_last_not_of(WHITESPACE);
    return (end == std::string::npos) ? "" : s.substr(0, end + 1);
}
 
std::string trim(const std::string &s) {
    return rtrim(ltrim(s));
}

//###################################################

void parsesvg::parseToJSON(std::string _fileName)
{
  std::ifstream in(_fileName + ".svg");
  std::ofstream out(_fileName + ".paths");

  if(in.fail())
  {
    std::cout << "[Error] Can not open in file " << _fileName << ".svg!\n";
    exit(2);
  }
  if(out.fail())
  {
    std::cout << "[Error] Can not open/create out file " << _fileName << ".json!\n";
  }
  
  std::string line;
//  out << "{\n  \"paths\":[\n";
  while(std::getline(in, line))
  {
    std::stringstream ss(trim(line));
    std::string token;
    ss >> token;
    if(token == "<path")
    {
      ss >> token;
      out << token << " ";
      ss >> token;
      out << token << " ";
      std::getline(ss,token);
      out << token.substr(0, token.size() - 5) << "\n"; 
    }
  }
  //JSON output variation
//  while(std::getline(in, line))
//  {
//    std::stringstream ss(trim(line));
//    std::string token;
//    ss >> token;
//    if(token == "<path")
//    {
//      out << "    {\n";
//      ss >> token;
//      out << "      \"id\":" << token.substr(3) << ",\n";
//      ss >> token;
//      out << "      \"class\":" << token.substr(6) << ",\n";
//      std::getline(ss, token);
//      out << "      \"d\":" << <token.substr(3, token.size() - 5) < "\n";
//      out << "    },\n";
//    }
//  } 
//  out << "  ]\n}";
 
  std::cout << "[Success] Output was made to " << _fileName << ".json\n";
  in.close();
  out.close();
}

int main()
{
  parsesvg parser;
  std::string file;
  std::cout << "[Log] Please enter a svg file to parse i.e. the path without the .svg: ";
  std::cin >> file;
  parser.parseToJSON(file);
  return 0;
}
