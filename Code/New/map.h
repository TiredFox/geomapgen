#ifndef STRUCTS_H
#define STRUCTS_H

#include <string>
#include <vector>

/**
 * @brief The options that be given for a specific map including color, and
 * title
 */
class map {
private:
  std::string title;
  std::string location;
  std::vector<std::string> colors;

public:
  // contructor
  map() {
    title = "Geomap";
    location = "Unknown";
  }

  /**
   * @brief A getter to return the title of a specific map
   * @return The string value of the title
   */
  std::string getTitle() { return title; }

  /**
   * @brief A getter for the location of the specific map
   * @return The string value of the maps location
   */
  std::string getLocation() { return location; }

  /**
   * @brief A getter for the colors of the map
   * @return The string vector value of the list of colors
   */
  std::vector<std::string> getColors() { return colors; }

  /**
   * @brief A setter that will change the title of the current map
   * @_title The string value of the new title
   */
  void setTitle(std::string _title) { title = _title; }

  /**
   * @brief A setter that will change the location value of the current map
   * @_location The string value of the new location
   */
  void setLocation(std::string _location) { location = _location; }

  /**
   * @brief A function to add a color to the color vector
   * @color The color that is to be added to the list of colors
   */
  void addColor(std::string color) { colors.push_back(color); }

  /**
   * @brief Will change the list of colors to the incoming list
   * @_colors The new list of colors that will be used
   */
  void changeColors(std::vector<std::string> _colors) { colors = _colors; }

  /**
   * @brief a print function that will format and print all information of the
   * map
   * @return The formatted string value of the maps information
   */
  std::string print() {
    std::string output;
    output += getTitle() + " , " + getLocation() + " , " + " Colors [";

    if (getColors().size() > 0) {
      output += getColors()[0];
      for (int i = 1; i < getColors().size(); i++) {
        output += " , " + getColors()[i];
      }
    }
    output += "]\n";
    return output;
  }
};

#endif // STRUCTS_H
